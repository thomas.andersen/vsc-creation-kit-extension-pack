# Creation Kit Extension Pack for VS Code

## Included Extensions

### [Papyrus](https://marketplace.visualstudio.com/items?itemName=plankton020.papyrus) 

Papyrus script support for vs Code.

### [Papyrus Compiler](https://marketplace.visualstudio.com/items?itemName=mr-andersen.vs-papyrus-compiler) 

Adds commands for running the Papyrus compiler from VS Code

## Contributing

Please submit issues with the extensions against the extension itself.

To get a new package included in this extension pack, feel free to submit a PR.